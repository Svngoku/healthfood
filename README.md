# foodapi

## Project setup

```sh
yarn install
```

### Compiles and hot-reloads for development

```sh
yarn serve
```

### Compiles and minifies for production

```sh
yarn build
```

### Lints and fixes files

```sh
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Tools

- Axios

- MongoDB

- Tailwind CSS

- VueJS TailWind

- Vue Router

- Lunr.js

```md
Lunr.js is a small, full-text search library for use in the browser. It indexes JSON documents and provides a simple search interface for retrieving documents that best match text queries.
```

## Datasets

### Edamam Food and Grocery Database API Documentation

```md

This API provides you with tools to find nutrition and diet data for generic foods, packaged foods and restaurant meals. In addition it employs NLP (Natural Language Processing) which allows for extraction of food entities from unstructured text. Covered Use Cases: Search for a food by keyword, food name or UPC/Barcode Sourcing of nutrition facts for a given food, including: macro and micro nutrients, allergen labels, lifestyle and health labels Search for a food by given nutrient quantity for 28 nutrients.
```

#### Covered Use Cases

Full analysis of food recipes in real time – entity extraction, measure and quantity extraction with computation of the aplicable nutrition for the recipe and applicable health and diet labels. Finally it adjusts quantity for certain ingredients to account for the cooking pocess. For example it calculates oil absorption for fried recipes, excludes solids from stock and broth recipes, calculates marinate absorption for marinates and much more.
Extraction of food entities with mesures and quantities from unstructured text
Usage in chat bots transcribing natural speach to text.
