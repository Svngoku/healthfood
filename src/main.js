import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router'
import './registerServiceWorker';
// import Lunr from "lunr";

// Config
Vue.use(VueSpinnersCss);
Vue.use(VueRouter);
Vue.config.productionTip = false;

// The Vue Instance
new Vue({
  render(h) {
    return h(App);
  },
}).$mount('#app');
